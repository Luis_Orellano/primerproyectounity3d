﻿using System.Drawing;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonajeController : MonoBehaviour
{
    public GameObject camara;
    public GameObject positionCamara;
    public GameObject targetPos;
    public GameObject ojosPos;
    public GameObject obstaclePos;
    Vector3 newPos;
    bool moving;
    bool isGrounded;

    public Rigidbody bicho;

    [SerializeField]
    private bool voiceReconiced;

    public float velocidadMovimientoPlayer;
    public float velocidadRotacionPlayer;

    // Start is called before the first frame update
    void Start()
    {
        moving = false;
        isGrounded = false;
    }

    // Update is called once per frame
    void Update()
    {
        // movimientoPersonaje();
        // rotarPersonaje();

        if(voiceReconiced){
            rotarPersonaje();
        }

        if(moving){
            moverPersonaje();
        }

        if(isGrounded){
            personajeJump();
        }

        // if(Input.GetMouseButtonDown(0)){
        //     RaycastHit hitSuelo;
        //     RaycastHit hitObstacles;

        //     Ray ray = camara.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);

        //     if(Physics.Raycast(ray, out hitSuelo)){ //Ha colisionado con algo
        //         if(hitSuelo.collider.CompareTag("suelo")){
        //             newPos = hitSuelo.point;
        //             targetPos.transform.position = newPos;
        //             positionCamara.transform.SetParent(null);
        //             transform.LookAt(new Vector3(newPos.x, transform.position.y, newPos.z));
        //             positionCamara.transform.SetParent(transform);

        //             if(Physics.Raycast(ojosPos.transform.position, newPos-ojosPos.transform.position, out hitObstacles, Vector3.Distance(transform.position, newPos))){
        //                 if(hitObstacles.collider.CompareTag("obstaculo")){
        //                     obstaclePos.transform.position = hitObstacles.point;
        //                     Debug.Log("Estas tocando un obstaculo!");
        //                 } else {
        //                     moving = true;
        //                 }
        //             } else {
        //                 moving = true;
        //             }
        //         }
        //     }
        // }

        // if(moving == true){
        //     transform.Translate(new Vector3(0, 0, velocidadMovimientoPlayer * Time.deltaTime));
        //     if(Vector3.Distance(transform.position, newPos) < velocidadMovimientoPlayer * Time.deltaTime){
        //         moving = false;
        //     }
        // }

        
    }

    private void movimientoCamaraPersonaje()
    {
        camara.transform.position = Vector3.Lerp(camara.transform.position, positionCamara.transform.position, 0.1f);
    }


    private void moverPersonaje() {
        transform.Translate(new Vector3(0, 0, 1f * Time.deltaTime));
    }

    // CODIGO PARA MOVER CON LAS TENCLAS A W S D
    private void rotarPersonaje()
    {  
        Debug.Log("HOLA");
        transform.Rotate(new Vector3(0f, 0.1f, 0f));
        // if (Input.GetKey(KeyCode.D)){
        //     positionCamara.transform.SetParent(null);
        //     transform.Rotate(new Vector3(0, velocidadRotacionPlayer * Time.deltaTime, 0));
        //     positionCamara.transform.SetParent(transform);
        // }
        // else{
        //     if (Input.GetKey(KeyCode.A)){
        //         positionCamara.transform.SetParent(null);
        //         transform.Rotate(new Vector3(0, -velocidadRotacionPlayer * Time.deltaTime, 0));
        //         positionCamara.transform.SetParent(transform);
        //     }
        // }
    }

    public void rotation() {
        voiceReconiced = true;
        Debug.Log("HOLA");
    }

    public void stop() {
        voiceReconiced = false;
    }

    public void move() {
        moving = true;
    }

    public void stopMove() {
        moving = false;
    }

    private void personajeJump() {
        bicho.AddForce(new Vector3(0, 1000.0f * Time.deltaTime, 0), ForceMode.Impulse);
        isGrounded = false;
    }

    public void jumping() {
        isGrounded = true;
        
        
    }

    
    
    // private void movimientoPersonaje()
    // {
    //     if (Input.GetKey(KeyCode.W)){
    //         transform.Translate(new Vector3(0, 0, velocidadMovimientoPlayer * Time.deltaTime));
    //     }
    //     else{
    //         if (Input.GetKey(KeyCode.S)){
    //             transform.Translate(new Vector3(0, 0, -velocidadMovimientoPlayer * Time.deltaTime));
    //         }
    //     }
    // }
}
