﻿using System.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using TMPro;

public class SpeechController : MonoBehaviour
{
    public TextMeshProUGUI myTMP;
    private KeywordRecognizer recognizerWord;
    private ConfidenceLevel confidence = ConfidenceLevel.Low;
    private Dictionary<string, Action> wordAction = new Dictionary<string, Action>(); 

    private PersonajeController personajeCtrl;

    private delegate void Accion();

    void Start()
    {
        personajeCtrl = GetComponent<PersonajeController>();

        wordAction.Add("rotar", personajeCtrl.rotation);
        wordAction.Add("detener", personajeCtrl.stop);
        wordAction.Add("caminar", personajeCtrl.move);
        wordAction.Add("dejardecaminar", personajeCtrl.stopMove);
        wordAction.Add("saltar", personajeCtrl.jumping);

        recognizerWord = new KeywordRecognizer(wordAction.Keys.ToArray(), confidence);
        recognizerWord.OnPhraseRecognized += OnKeywordsRecognized;
        recognizerWord.Start();
    }

    void OnDestroy() {
        if(recognizerWord != null && recognizerWord.IsRunning){
            recognizerWord.Stop();
            recognizerWord.Dispose();
        }
    }

    private void OnKeywordsRecognized(PhraseRecognizedEventArgs args){
        myTMP.text = args.text;
        wordAction[args.text].Invoke();
    }

}
